public class AreaComputations
{
	public static double areaSquare(double length)
	{
		double result = Math.pow(length, 2);
		
		return result;
	}
	
	public double areaRectangle(double length, double width)
	{
		double result = length * width;
		
		return result;
	}
}