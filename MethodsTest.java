public class MethodsTest
{
    public static void main(String[] args) 
    {
        int x =10;

        methodOneInputNoReturn(10);
        methodOneInputNoReturn(x);
        methodOneInputNoReturn(x + 50);

		methodTwoInputNoReturn(5, 2.5);
		
		int methodResult = methodNoInputReturnInt();
		
		System.out.println("The method methodNoInputReturnInt returned the value: " + methodResult);
		
		double sqrtResult = sumSquareRoot(6, 3);
		
		System.out.println("The square root method returned the following value: " + sqrtResult);
		
		String s1 = "hello";
		String s2 = "goodbye";
		
		System.out.println("The length of the string " + s1 + " is: " + s1.length());
		System.out.println("The length of the string " + s2 + " is: " + s2.length());
		
		System.out.println("The result of the method addOne is " + SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		
		
		
		System.out.println("The result of the method addTwo is " + sc.addTwo(50));
	}

    public static void methodNoInputNoReturn()
    {
        System.out.println("I'm in a method that takes no input and returns nothing,");
    }

    public static void methodOneInputNoReturn(int number)
    {
        System.out.println("Inside the method one input no return. Number:" + number);
    }

    public static void methodTwoInputNoReturn(int number, double otherNumber)
    {
        System.out.println("Inside the method two input no return. Numbers: " + number + " and " + otherNumber);
    }
	
	public static int methodNoInputReturnInt()
	{
		return 6;
	}
	
	public static double sumSquareRoot(int num1, int num2)
	{
		double result = Math.sqrt(num1 + num2);
		return result;
	}
}