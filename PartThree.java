import java.util.Scanner;

public class PartThree
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter the length of the square's side.");
		double lengthSqr = input.nextDouble();
		while(lengthSqr<=0)
		{
			System.out.println("Length must be positive. Try again.");
			lengthSqr = input.nextDouble();
		}
		
		System.out.println("Enter the length of the rectangle's side.");
		double lengthRec = input.nextDouble();
		while(lengthRec<=0)
		{
			System.out.println("Length must be positive. Try again.");
			lengthRec = input.nextDouble();
		}
		
		System.out.println("Enter the width of the rectangle's side.");
		double widthRec = input.nextDouble();
		while(widthRec<=0)
		{
			System.out.println("Width must be positive. Try again.");
			widthRec = input.nextDouble();
		}
		
		AreaComputations calc = new AreaComputations();
		
		double areaSqr = AreaComputations.areaSquare(lengthSqr);
		double areaRec = calc.areaRectangle(lengthRec, widthRec);
		
		System.out.println("The area of the square is " + areaSqr);
		System.out.println("The area of the rectangle is " + areaRec);
	}
}